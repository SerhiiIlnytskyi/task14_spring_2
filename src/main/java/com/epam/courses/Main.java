package com.epam.courses;

import com.epam.courses.anotherpackage.BeanFirstCollector;
import com.epam.courses.anotherpackage.BeanSecondCollector;
import com.epam.courses.anotherpackage.OtherBeanD;
import com.epam.courses.anotherpackage.OtherBeanE;
import com.epam.courses.anotherpackage.OtherBeanF;
import com.epam.courses.config.FirstConfig;
import com.epam.courses.config.SecondConfig;
import com.epam.courses.config.ThirdConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

  public static void main(String[] args) {
//    ApplicationContext context = new AnnotationConfigApplicationContext(FirstConfig.class,
//        SecondConfig.class, ThirdConfig.class);
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.getEnvironment().setActiveProfiles("component");
    context.register(ThirdConfig.class);
    context.refresh();
    for (String beanName : context.getBeanDefinitionNames()) {
      System.out.println(context.getBean(beanName).getClass().toString());
    }
    System.out.println(context.getBean(OtherBeanD.class).toString());
    System.out.println(context.getBean(OtherBeanE.class).toString());
    System.out.println(context.getBean(OtherBeanF.class).toString());

    context.getBean(BeanFirstCollector.class).printBeans();
    context.getBean(BeanSecondCollector.class).printBeans();

  }
}
