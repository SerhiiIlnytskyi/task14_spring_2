package com.epam.courses.anotherpackage;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanFirstCollector {

  @Autowired
  private List<BaseBean> beans;

  public void printBeans() {
    System.out.println("BEANS FROM FIRST COLLECTOR");
    for (BaseBean bean : beans) {
      System.out.println(bean.toString());
    }
    System.out.println("==========================");
  }

}
