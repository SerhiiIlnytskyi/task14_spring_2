package com.epam.courses.anotherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanSecondCollector {

  @Autowired
  private BaseBean bean1;

  @Autowired
  @Qualifier("otherBeanA")
  private BaseBean bean2;

  @Autowired
  @Qualifier("otherBeanC")
  private BaseBean bean3;

  @Autowired
  @Qualifier("otherBeanD")
  private BaseBean bean4;

  @Autowired
  @Qualifier("otherBeanE")
  private BaseBean bean5;

  @Autowired
  @Qualifier("otherBeanF")
  private BaseBean bean6;

  public void printBeans() {
    System.out.println("BEANS FROM SECOND COLLECTOR");
    System.out.println(bean1);
    System.out.println(bean2);
    System.out.println(bean3);
    System.out.println(bean4);
    System.out.println(bean5);
    System.out.println(bean6);
    System.out.println("===========================");
  }


}
