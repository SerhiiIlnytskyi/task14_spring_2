package com.epam.courses.anotherpackage;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class OtherBeanA implements BaseBean{

}
