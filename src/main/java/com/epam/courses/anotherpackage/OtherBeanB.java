package com.epam.courses.anotherpackage;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(6)
@Primary
@Scope("singleton")
public class OtherBeanB implements BaseBean{

}
