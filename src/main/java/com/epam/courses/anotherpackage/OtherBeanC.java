package com.epam.courses.anotherpackage;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("someBean")
@Order(4)
@Scope("prototype")
public class OtherBeanC implements BaseBean{

}
