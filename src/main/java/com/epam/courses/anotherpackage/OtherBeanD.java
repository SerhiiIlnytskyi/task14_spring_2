package com.epam.courses.anotherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class OtherBeanD implements BaseBean {

  @Autowired
  private BaseBean otherBeanA;

  @Autowired
  private BaseBean otherBeanB;

  @Autowired
  @Qualifier("someBean")
  private BaseBean bean;

  @Override
  public String toString() {
    return "OtherBeanD{" +
        "otherBeanA=" + otherBeanA +
        ", otherBeanB=" + otherBeanB +
        ", bean=" + bean +
        '}';
  }
}
