package com.epam.courses.anotherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(5)
public class OtherBeanE implements BaseBean {

  private BaseBean otherBeanA;

  private BaseBean otherBeanB;

  private BaseBean bean;

  @Autowired
  public OtherBeanE(BaseBean otherBeanA, BaseBean otherBeanB,
      @Qualifier("someBean") BaseBean bean) {
    this.otherBeanA = otherBeanA;
    this.otherBeanB = otherBeanB;
    this.bean = bean;
  }

  @Override
  public String toString() {
    return "OtherBeanE{" +
        "otherBeanA=" + otherBeanA +
        ", otherBeanB=" + otherBeanB +
        ", bean=" + bean +
        '}';
  }
}
