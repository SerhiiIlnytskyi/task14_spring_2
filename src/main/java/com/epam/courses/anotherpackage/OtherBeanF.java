package com.epam.courses.anotherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class OtherBeanF implements BaseBean {

  private BaseBean otherBeanA;

  private BaseBean otherBeanB;

  private BaseBean bean;

  @Autowired
  public void setOtherBeanA(OtherBeanA otherBeanA) {
    this.otherBeanA = otherBeanA;
  }

  @Autowired
  public void setOtherBeanB(OtherBeanB otherBeanB) {
    this.otherBeanB = otherBeanB;
  }

  @Autowired
  @Qualifier("someBean")
  public void setBean(OtherBeanC bean) {
    this.bean = bean;
  }

  @Override
  public String toString() {
    return "OtherBeanF{" +
        "otherBeanA=" + otherBeanA +
        ", otherBeanB=" + otherBeanB +
        ", bean=" + bean +
        '}';
  }
}
