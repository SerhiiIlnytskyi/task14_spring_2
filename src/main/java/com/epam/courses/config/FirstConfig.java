package com.epam.courses.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan("com.epam.courses.beans1")
@Profile("component")
public class FirstConfig {

}
