package com.epam.courses.config;

import com.epam.courses.beans3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScans({
    @ComponentScan("com.epam.courses.beans2"),
    @ComponentScan(basePackages = "com.epam.courses.beans3",
        excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE,
            classes = BeanE.class))})
@Profile("component")
public class SecondConfig {

}
