package com.epam.courses.config;

import com.epam.courses.anotherpackage.BaseBean;
import com.epam.courses.anotherpackage.OtherBeanC;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan("com.epam.courses.anotherpackage")
@Profile("component")
public class ThirdConfig {

  @Profile("bean")
  @Bean
  public BaseBean beanBeanBean() {
    return new OtherBeanC();
  }
}
